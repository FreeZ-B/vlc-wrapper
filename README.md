//TODO: Update README
# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

This is view-wrapper for Android libVLC. It's currently base on libVLC_3.0.0.
This wrapper-view contains surface to display video output, basic controls (which you can actually re-init with your), loading-buffering-seeking-hiding-showing fade-animations.

### How do I get set up? ###

* Make sure to add this to your main project build.gradle file:

```
#!groovy


allprojects {
    repositories {
        // Your other repos

        flatDir {
            dirs project(':<path_to_module_folder>:vlc-wrapper').file('libs')
        }
    }
}
```