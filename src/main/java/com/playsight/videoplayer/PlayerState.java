package com.playsight.videoplayer;

/**
 * Created by Pavel Barmin on 14.12.15
 */
interface PlayerState {
    int IDLE = 0;
    int OPENING = 1;
    int BUFFERING = 2;
    int PLAYING = 3;
    int PAUSED = 4;
    int STOPPING = 5;
    int ENDED = 7;
    int ERROR = 8;
}
