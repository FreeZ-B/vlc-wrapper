package com.playsight.videoplayer;

/**
 * Created by Pavel Barmin on 23.10.15
 */
public interface IMediaPlayer {
    long getCurrentPosition();
    void addOnTimeListener(VideoPlayer.OnTimeUpdatedListener listener, long intervalMilis, final String tag);
}
