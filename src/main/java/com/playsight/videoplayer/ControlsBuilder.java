package com.playsight.videoplayer;

import android.support.annotation.IdRes;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pavel Barmin on 14.12.15
 */
public class ControlsBuilder {

    protected List<RewinderControl> mRewinders;

    protected int mCurrentTimeTextViewId;
    protected int mOverallTimeTextViewId;
    protected int mSeekbarJoggerId;
    protected int mTimeStampTextViewId;
    protected int mTimeStampThumbsViewId;

    public ControlsBuilder setCurrentTimeTextViewId(@IdRes int id) {
        this.mCurrentTimeTextViewId = id;
        return this;
    }

    public ControlsBuilder setOverallTimeTextViewId(@IdRes int id) {
        this.mOverallTimeTextViewId = id;
        return this;
    }

    public ControlsBuilder setSeekbarJoggerId(@IdRes int id) {
        this.mSeekbarJoggerId = id;
        return this;
    }

    public ControlsBuilder setTimeStampTextViewId(@IdRes int id) {
        this.mTimeStampTextViewId = id;
        return this;
    }

    public ControlsBuilder setVideoThumbsViewId(@IdRes int id) {
        this.mTimeStampThumbsViewId = id;
        return this;
    }

    public ControlsBuilder addRewinder(RewinderControl rc) {
        if (rc == null) {
            return this;
        }
        if (mRewinders == null) {
            mRewinders = new ArrayList<>();
        }

        mRewinders.add(rc);
        return this;
    }
}
