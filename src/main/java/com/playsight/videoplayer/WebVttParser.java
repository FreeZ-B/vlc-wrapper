package com.playsight.videoplayer;

import android.text.Html;


import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

/**
 * Created by v_alekseev on 13.06.17.
 */

public class WebVttParser {


    private ArrayList<Phrase> subtitles = new ArrayList<Phrase>();
    private String mCurrentCharsetName;
    private long mSubtitleDalay;

    private SubtitleListener listener;

    public interface SubtitleListener {
        void VttFilePrepared();
    }

    WebVttParser(String webVttUrl, SubtitleListener listener) {
        this.listener = listener;
        try {
            prepareSubtitles(new URL(webVttUrl));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void prepareSubtitles(final URL url) throws IOException {
        mCurrentCharsetName = null;
        // todo add some listenere for detecting when we catch some exception
        (new Thread() {
            @Override
            public void run() {
                try {
                    HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                    connection.setDoInput(true);
                    InputStream in = new BufferedInputStream(connection.getInputStream());
                    readSubtitle(in);
                } catch (Exception ex) {

                }
            }
        }).start();
    }

    public InputStream readSubtitle(InputStream in) throws IOException {
        this.subtitles.clear();
        InputStreamReader inStream;
        if (mCurrentCharsetName == null)
            inStream = new InputStreamReader(in);
        else {
            if (mCurrentCharsetName.equalsIgnoreCase("MACCYRILLIC")) {
                inStream = new InputStreamReader(in, "cp1256");
            } else
                inStream = new InputStreamReader(in, mCurrentCharsetName);
        }

        final BufferedReader bufferedReader = new BufferedReader(inStream);
        (new Thread() {
            @Override
            public void run() {
                try {
                    readSubtitle(bufferedReader);
                } catch (Exception e) {
                    e.printStackTrace();
                    // todo
                }
            }
        }).start();
        return in;
    }

    private void readSubtitle(BufferedReader bufferedReader) throws Exception {
        boolean isReadSomethin = false;
        String num = "";
        String time = "";
        ArrayList<String> text = new ArrayList<String>();
        String bufStr = "";
        while (true) {
            bufStr = bufferedReader.readLine();
            if (bufStr == null) {
                break;
            }
            if (bufStr.equals("")) {
                if (isReadSomethin) {
                    try {
                        final Phrase phrase = new Phrase(num, time, text, getmSubtitleDalay());
                        Sync(new SynhronizerWorker() {
                            @Override
                            public void work() {
                                WebVttParser.this.subtitles.add(phrase);
                            }
                        });

                    } catch (Exception e) {
                        // continue;
                        e.printStackTrace();
                    }
                    isReadSomethin = false;
                    num = "";
                    time = "";
                    text.clear();
                    continue;

                }
            } else {
                isReadSomethin = true;
            }
//            String firstChar = bufStr.substring(0, 1);
//            if (num.equals("") && !firstChar.equals("0")) {
//                num = bufStr;
//            } else if (time.equals("")) {
//                time = bufStr;
//            } else {
//                text.add(bufStr);
//            }

            if (time.equals("")) {
                time = bufStr;
            } else {
                text.add(bufStr);
            }
        }
        listener.VttFilePrepared();
    }

    public String getSubtitle(long timeInMilliseconds) {
        try {
            final long position = timeInMilliseconds;

            for (final Phrase phrase : subtitles) {
                if (phrase.isCanShow(position)) {
                    final Phrase phraseForShow = phrase;
                    String subtitle = String.valueOf(Html.fromHtml(phraseForShow.getContent()));

                    return subtitle;
                }
            }

            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public long getmSubtitleDalay() {
        return mSubtitleDalay;
    }

    public class Phrase {
        private String content;
        private final long begin;
        private final long end;
        private String num;
        private long delay;

        public Phrase(String num, String time, ArrayList<String> content, long _delay) throws Exception {
            if (!num.endsWith("")) {
                // todo read num??
            }
            delay = _delay;
            this.num = num;
            String[] timePut = time.split("-->");
            if (timePut.length != 2)
                throw new Exception("can`t parse time");
            this.begin = parseTime(timePut[0]);
            this.end = parseTime(timePut[1]);

            this.content = "";
            for (String str : content)
                this.content += str + "\n";
        }

        private long parseTime(String time) {
            time = time.replaceAll(" ", "");
            String[] timePut = time.split(":");
            long result = 0;
            result += Integer.parseInt(timePut[0]) * 1000 * 60 * 60;
            result += Integer.parseInt(timePut[1]) * 1000 * 60;
            String[] sec_milisec = timePut[2].split("\\.");
            result += Integer.parseInt(sec_milisec[0]) * 1000;
            result += Integer.parseInt(sec_milisec[1]);
            return result;
        }

        public boolean isCanShow(long nowtime) {
            return ((begin <= (nowtime + delay)) && (end > (nowtime + delay)));
        }

        public String getContent() {
            return content;
        }

        public String generateTimeString() {
            return getTimeStr(begin) + " --> " + getTimeStr(end);
        }

        private String getTimeStr(long _ms) {
            long secFull = _ms / 1000;
            long minFull = secFull / 60;
            long hours = minFull / 60;
            long ms = _ms % 1000;

            String strHours = "" + hours;
            String strMin = "" + (minFull % 60);
            String strSec = "" + (secFull % 60);

            return strHours + ":" + strMin + ":" + strSec + "," + ms;
        }

        public String getNum() {
            return num;
        }
    }

    private synchronized void Sync(SynhronizerWorker task) {
        task.work();
    }

    private interface SynhronizerWorker {
        public void work();
    }

}
