package com.playsight.videoplayer;

import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;

/**
 * Created by Pavel Barmin on 14.12.15
 */
public class MediaPlayer extends org.videolan.libvlc.MediaPlayer {
    public MediaPlayer(LibVLC libVLC) {
        super(libVLC);
    }

    public MediaPlayer(Media media) {
        super(media);
    }

    @Override
    protected synchronized Event onEventNative(int eventType, long arg1, float arg2) {
        final Event event = super.onEventNative(eventType, arg1, arg2);
        if (event == null){
            return new PlayerEvent(eventType);

        } else {
            return event;
        }
    }
}
