package com.playsight.videoplayer;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.SurfaceTexture;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.TextureView;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.RelativeLayout;

import com.pnikosis.materialishprogress.ProgressWheel;

import org.videolan.libvlc.IVLCVout;
import org.videolan.libvlc.LibVLC;
import org.videolan.libvlc.Media;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;

/**
 * Created by Pavel Barmin on 08.12.15
 */
public class VideoPlayer extends RelativeLayout implements IMediaPlayer {

    private static final String TAG = "VLC Wrapper";

    private static final String TAG_BUFFERING = "buffering";

    private static final long DEFAULT_BUFFERING_TICK = 2500l;
    private static final long DEFAULT_CACHING_MS = 2000l;

    private static final int VOLUME_MAX = 100;
    private static final int VOLUME_MUTE = 0;

    private static final Handler TIME_THREAD = new Handler();
    private final Handler mHanler;
    private final int[] fSize = new int[]{1280, 720};
    private final long[] vidBounds = new long[]{0, 0};
    protected float speedPercentage = 1f;
    private TextureView mDisplayView;
    private View mLoader;
    private VideoControls mControls;
    private LibVLC mVLC;
    private MediaPlayer mPlayer;
    private Uri mCurrentMedia;
    private boolean allowHardwareAcceleration = true;
    private boolean isWaitinfForDisplay = true;
    private boolean startPaused = false;
    private boolean mIsSeeking = false;
    private boolean mRequestLoader = false;
    private boolean forceHideControls = false;
    private boolean lockControls = false;
    private boolean forceUseLoader = true;
    private long mStartTime;
    private long lastKnownPlaybackTime;
    private long lastBufferedTime;
    private HashMap<String, TimeUpdater> mTimeUpdaters = new HashMap<>();
    private boolean startUp = true;
    private PlaybackListener mPlaybackListener;
    private OnErrorListener mErrorListener;
    private OnOpeningListener mOpeningListener;
    private boolean hwErrorHappened = false;
    private boolean mIsMuted = false;
    private Bitmap frame;
    private boolean isFrameSizeLocked = true;
    private boolean isSurfaceRecreated = false;
    private long timeOfLastBufferedTime = 0;
    private VideoControls.ControlsListener mControlsListener = new VideoControls.ControlsListener() {
        @Override
        public boolean onJoggerChanged(float newPercentage) {
            // Here we receive percentage related to A-B bounds.
            // We need to convert it to percentage in whole video
            if (mPlayer != null && (vidBounds[0] != 0 || vidBounds[1] != mPlayer.getLength())) {
                long positionInVideo = (long) ((vidBounds[1] - vidBounds[0]) * newPercentage + vidBounds[0]);
                newPercentage = positionInVideo / (float) mPlayer.getLength();
            }

            if (mPlayer != null) {
                if (mPlayer.getPlayerState() == PlayerState.IDLE) {
                    mPlayer.play();
                    mPlayer.setPosition(newPercentage);

                } else if (mPlayer.getPlayerState() != PlayerState.ERROR && mPlayer.getPlayerState() != PlayerState.ENDED
                        && mPlayer.isSeekable()) {
                    rewind();

                    if (mPlaybackListener != null) {
                        mPlaybackListener.onSeekStart(mPlayer.getTime());
                        mPlaybackListener.onSeekEnd((long) (mPlayer.getLength() * newPercentage));
                    }
                    mPlayer.setPosition(newPercentage);

                } else {
                    return false;
                }

                return true;

            } else {
                return false;
            }
        }

        @Override
        public void onRewind(long ms) {
            if (mPlayer != null && mPlayer.isSeekable()) {
                final long newSkipTime = mPlayer.getTime() + ms;
                if (newSkipTime >= 0 && newSkipTime < mPlayer.getLength()) {
                    seekTo(newSkipTime);
                }
            }
        }
    };
    private boolean isPlayingStarted;
    private float[] mDisplayOrigins = new float[]{0, 0};
    private IVLCVout.Callback mVLCVCallback = new IVLCVout.Callback() {
        @Override
        public void onNewLayout(IVLCVout vout, int width, int height, int videoWidth, int videoHeight, int sarNum, int sarDen) {
            Log.i(TAG, String.format("Frame size: %dx%d", videoWidth, videoHeight));

            // Resizing
            if (mDisplayView != null) {
                float videoRatio = videoWidth / (float) videoHeight;

                int newDisplayWidth;
                int newDisplayHeight;

                final int displayWidth = mDisplayView.getWidth();
                final int displayHeight = mDisplayView.getHeight();

                if (videoWidth > videoHeight) {
                    // Widescreen
                    newDisplayWidth = displayWidth;
                    newDisplayHeight = (int) (newDisplayWidth / videoRatio);

                } else if (videoHeight > videoWidth) {
                    // Portrait
                    newDisplayHeight = displayHeight;
                    newDisplayWidth = (int) (newDisplayHeight * videoRatio);

                } else {
                    // Square
                    newDisplayWidth = displayWidth;
                    newDisplayHeight = displayHeight;
                    // TODO
                }

                RelativeLayout.LayoutParams params = (LayoutParams) mDisplayView.getLayoutParams();
                params.width = newDisplayWidth;
                params.height = newDisplayHeight;
                mDisplayView.setLayoutParams(params);

                // Center
//                if (displayWidth > newDisplayWidth) {
//                    mDisplayView.setX((displayWidth / 2f) - newDisplayWidth / 2f);
//                }
//                if (displayHeight > newDisplayHeight) {
//                    mDisplayView.setY((displayHeight / 2f) - newDisplayHeight / 2f);
//                }
                invalidate();

                mDisplayOrigins[0] = mDisplayView.getX();
                mDisplayOrigins[1] = mDisplayView.getY();
            }
        }

        @Override
        public void onSurfacesCreated(IVLCVout ivlcVout) {
        }

        @Override
        public void onSurfacesDestroyed(IVLCVout ivlcVout) {
        }

        @Override
        public void onHardwareAccelerationError(IVLCVout vlcVout) {
            mHWErrorListener.onNativeCrash();
        }
    };
    private TextureView.SurfaceTextureListener mTextureListener = new TextureView.SurfaceTextureListener() {
        @Override
        public void onSurfaceTextureAvailable(SurfaceTexture surfaceTexture, int i, int i1) {
            if (isWaitinfForDisplay) {
                isWaitinfForDisplay = false;
                if (mPlayer != null) {
                    Log.d(TAG, "Video OUT ready - setting to player");
                    setVideoOutput();
                }
            }
        }

        @Override
        public void onSurfaceTextureSizeChanged(SurfaceTexture surfaceTexture, int i, int i1) {
        }

        @Override
        public boolean onSurfaceTextureDestroyed(SurfaceTexture surfaceTexture) {
            return false;
        }

        @Override
        public void onSurfaceTextureUpdated(SurfaceTexture surfaceTexture) {
            if (mIsSeeking) {
                mIsSeeking = false;
                showLoader(false);
            }
        }
    };
    private MediaPlayer.EventListener mPEventListener = new MediaPlayer.EventListener() {
        @Override
        public void onEvent(MediaPlayer.Event event) {
            switch (event.type) {
                case MediaPlayer.Event.Opening:
                    break;

                case MediaPlayer.Event.Playing:
                    Log.i(TAG, "Playing video");
                    callOpeningListener();
                    isPlayingStarted = true;
                    // Let's check the state of the player after returning from outside
                    if (mPlayer == null) {
                        return;
                    }

                    if (startUp) {
                        mPlayer.setRate(speedPercentage);
                        if (mIsMuted) {
                            getHandler().postDelayed(new Runnable() {
                                @Override
                                public void run() {
                                    mute();
                                }
                            }, 300);
                        }
                        startUp = false;

                        if (vidBounds[1] == 0) {
                            vidBounds[1] = mPlayer.getLength();
                        }

                        if (vidBounds[0] > 0) {
                            mStartTime = vidBounds[0];
                        }
                    }

                    final long time = mStartTime;
                    if (mStartTime > 0) {
                        seekTo(mStartTime);
                        mStartTime = 0;
                    }

                    startCounters();
                    if (mPlaybackListener != null) {
                        mPlaybackListener.onStart(time > 0 ? time : getCurrentPosition());
                    }

                    if (mControls != null && mPlayer != null) {
                        mControls.setOverallTime(vidBounds[1]);
                        mControls.setMinimumTime(vidBounds[0]);
                        Log.d("!!!!!", "mPlayer.getMedia().getUri() " + mPlayer.getMedia().getUri());

//                        mControls.setVtt(mPlayer.getMedia().getUri());
//                        mControls.setVtt("https://playsightstaging.blob.core.windows.net/files/12_34_46_2__0__2L_1L_47.56.65.3.mp4");
                    }

//                    if (mControls != null && !mControls.isHidden()) {
//                        mControls.show(false);
//                    }

                    break;
                case MediaPlayer.Event.Paused:
                    if (!forceHideControls) {
                        showControls();
                    }

                    showLoader(false);

                    stopCounters(false);
                    if (mPlaybackListener != null) {
                        mPlaybackListener.onPause(getCurrentPosition());
                    }
                    break;
                case MediaPlayer.Event.Stopped:
                    if (!forceHideControls) {
                        showControls();
                    }

                    showLoader(false);

                    stopCounters(true);

                    if (mPlaybackListener != null) {
                        mPlaybackListener.onStop(mControls.getOverallTime());
                    }
                    break;

                case MediaPlayer.Event.EndReached:
                    if (mControls != null && mControls.isHidden() && !forceHideControls) {
                        if (mPlayer != null) {
                            mControls.updateCurrentTime(mPlayer.getLength());
                            mControls.updateJogger(1f);
                        }
                        showControls();
                    }

                    showLoader(false);

                    if (mPlaybackListener != null) {
                        mPlaybackListener.onStop(mControls.getOverallTime());
                    }
                    // Make sure to re-init player
                    startPaused = true;
                    createPlayer();
                    break;

                case MediaPlayer.Event.EncounteredError:
                    if (mErrorListener != null) {
                        mErrorListener.onError();
                    }
                    releaseAll();
                    break;

                case MediaPlayer.Event.TimeChanged:
                    lastKnownPlaybackTime = event.getTimeChanged();

                    if (mControls != null) {
                        mControls.updateCurrentTime(event.getTimeChanged());
                    }

                    if (lastKnownPlaybackTime >= vidBounds[1]) {
                        seekTo(vidBounds[0]);
                        pause();
                    }

                    break;
                case MediaPlayer.Event.PositionChanged:
                    if (mControls != null) {
                        float wholeVideoPercentage = event.getPositionChanged();
                        long positionInWholeVideo = (long) (mPlayer.getLength() * wholeVideoPercentage);
                        float percentageInBounds = (positionInWholeVideo - vidBounds[0]) / (float) (vidBounds[1] - vidBounds[0]);

                        mControls.updateJogger(percentageInBounds);
                    }
                    break;

                case MediaPlayer.Event.Vout:
                    Log.i(TAG, "Video Out");
                    callOpeningListener();
                    isPlayingStarted = true;

                    if (startPaused) {
                        startPaused = false;
                        pause();
                        break;
                    }
                    break;
                case MediaPlayer.Event.ESAdded:
                case MediaPlayer.Event.ESDeleted:
                case MediaPlayer.Event.PausableChanged:
                case MediaPlayer.Event.SeekableChanged:
                    // Ignore
                    break;
                default:
                    Log.i(TAG, String.format("Unknown event!\nType: %d", event.type));
            }
        }
    };
    private LibVLC.OnNativeCrashListener mHWErrorListener = new LibVLC.OnNativeCrashListener() {
        @Override
        public void onNativeCrash() {
            Log.e(TAG, "Native crash occured");
            if (!hwErrorHappened) {
                // Lets try play the video without it
                hwErrorHappened = true;
                startPaused = mPlayer == null || !mPlayer.isPlaying();

                Handler handler = getHandler();
                if (handler != null) {
                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            createPlayer();
                        }
                    });
                } else {
                    mPEventListener.onEvent(new PlayerEvent(PlayerEvent.EncounteredError));
                }

                mStartTime = lastKnownPlaybackTime;

            } else {
                mPEventListener.onEvent(new PlayerEvent(PlayerEvent.EncounteredError));
            }
        }
    };

    public VideoPlayer(Context context) {
        super(context);
        this.mHanler = new Handler();

        init(null);
    }

    public VideoPlayer(Context context, AttributeSet attrs) {
        super(context, attrs);
        this.mHanler = new Handler();

        init(attrs);
    }

    public VideoPlayer(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mHanler = new Handler();

        init(attrs);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public VideoPlayer(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mHanler = new Handler();

        init(attrs);
    }

    private void callOpeningListener() {
        if (mOpeningListener != null && !isPlayingStarted) {
            mOpeningListener.onOpeningStarted();
        }
    }

    private void init(AttributeSet attrs) {
        final Context c = getContext();
        mDisplayView = createTextureView();

        boolean allowLoader = true;
        int DEFAULT_LOADER_COLOR = Color.CYAN;
        final int loaderColor = DEFAULT_LOADER_COLOR;
        boolean allowControls = true;
        int controlsLayoutId = -1;
        if (attrs != null) {
            final TypedArray typedArray = c.obtainStyledAttributes(attrs, R.styleable.VideoPlayer);

            allowLoader = typedArray.getBoolean(R.styleable.VideoPlayer_loading_indicator_allow, allowLoader);
            if (allowLoader) {
                final int loaderId = typedArray.getResourceId(R.styleable.VideoPlayer_loading_indicator, -1);
                if (loaderId != -1) {
                    getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            getViewTreeObserver().removeOnGlobalLayoutListener(this);

                            final View probablyLoader = Utils.getTopLayout(VideoPlayer.this).findViewById(loaderId);
                            if (probablyLoader != null) {
                                mLoader = probablyLoader;

                            } else {
                                final float loaderSize = 50f;
                                final int size = (int) Utils.dpToPx(loaderSize, c);
                                ProgressWheel loader = new ProgressWheel(c);
                                RelativeLayout.LayoutParams loaderParams = new RelativeLayout.LayoutParams(size, size);
                                loaderParams.addRule(CENTER_IN_PARENT);
                                loader.setLayoutParams(loaderParams);
                                loader.setBarColor(loaderColor);
                                loader.setBarWidth((int) Utils.dpToPx(4f, c));
                                loader.setCircleRadius(size - loader.getBarWidth());
                                loader.spin();
                                mLoader = loader;
                                addView(mLoader);
                            }

                            if (mRequestLoader)
                                showLoader(true);
                        }
                    });
                }
            }

            allowControls = typedArray.getBoolean(R.styleable.VideoPlayer_controls_allow, allowControls);
            if (allowControls) {
                controlsLayoutId = typedArray.getResourceId(R.styleable.VideoPlayer_controls_layout, controlsLayoutId);
                if (controlsLayoutId != -1) {
                    final View controls = View.inflate(c, controlsLayoutId, null);
                    if (controls != null) {
                        mControls = new VideoControls(c, controlsLayoutId);
                    }
                }
            }

            typedArray.recycle();
        }

        if (allowControls) {
            if (mControls == null) {
                mControls = new VideoControls(c, controlsLayoutId);
            }
            mControls.setControlsListener(mControlsListener);
            addView(mControls.getTarget());
        }

        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mPlayer != null && mPlayer.getMedia() != null) {
                    // TODO: Add playback start in case of NONnull player && NULL media!
                    if (mPlayer.isPlaying()) {
                        pause();
                    } else {
                        play();
                    }
                }
            }
        });
    }

    private void createPlayer() {
        isPlayingStarted = false;
        // Clean previous
        if (mPlayer != null) {
            releaseAll();
            // Here we're resetting TextureView
            isWaitinfForDisplay = true;
            mDisplayView = createTextureView();
        }

        // Create LibVLC
        ArrayList<String> options = new ArrayList<>();
        options.add("--audio-time-stretch"); // time stretching
        options.add("--http-reconnect");
        options.add(String.format(Locale.US, "--network-caching=%d", DEFAULT_CACHING_MS)); // caching
        if (!allowHardwareAcceleration) {
            options.add("--nooverlay"); //disables hardware acceleration for the video output
        }
        mVLC = new LibVLC(options);

        addOnTimeListener(new OnTimeUpdatedListener() {
            @Override
            public void onTimeUpdated(long newTime) {
                if (mPlayer != null && mPlayer.isPlaying()) {
                    // Check buffering (dumm)
                    if (newTime == lastBufferedTime) {
                        showLoader(true);
                        if (mPlaybackListener != null && !mPlaybackListener.seekStarted && !mPlaybackListener.bufferingStarted) {
                            mPlaybackListener.onBufferingStart(newTime);
                        }

                    } else if (newTime != lastBufferedTime) {
                        showLoader(false);
                        if (mPlaybackListener != null && mPlaybackListener.bufferingStarted) {
                            mPlaybackListener.onBufferingEnd(newTime);
                        }
                    }
                    lastBufferedTime = newTime;

                }
            }
        }, DEFAULT_BUFFERING_TICK, TAG_BUFFERING, true);

        // Create media player
        mPlayer = new MediaPlayer(mVLC);
        mPlayer.setEventListener(mPEventListener);

        if (mDisplayView != null && mDisplayView.getSurfaceTexture() != null) {
            setVideoOutput();

        } else {
            isWaitinfForDisplay = true;
        }

        Media m;

//        mCurrentMedia = Uri.parse("smb://playsight:doit4me@titanium/e/Play_files/16-34-57_1977_11389__076__2L_1L_47.56.65.14_Court1_replay.mp4");
        String path = mCurrentMedia.toString();

        if (path.startsWith("http") || path.startsWith("smb")) {
            m = new Media(mVLC, mCurrentMedia);
        } else {
            m = new Media(mVLC, path);
        }
        mPlayer.setMedia(m);

        mPlayer.play();
        Log.d(TAG, "Playing video: " + path);
    }

    protected TextureView createTextureView() {
        if (mDisplayView != null) {
            removeView(mDisplayView);
            mDisplayView = null;
        }

        TextureView textureView = new TextureView(getContext());
        textureView.setKeepScreenOn(true);
        textureView.setSurfaceTextureListener(mTextureListener);
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT);
        layoutParams.addRule(CENTER_IN_PARENT);
        textureView.setLayoutParams(layoutParams);
        addView(textureView, 0);

        invalidate();

        return textureView;
    }

    protected void setVideoOutput() {
        final IVLCVout vout = mPlayer.getVLCVout();
        if (vout.areViewsAttached()) {
            vout.detachViews();
        }
        vout.setVideoView(mDisplayView);
        //vout.setSubtitlesView(mSurfaceSubtitles);
        vout.addCallback(mVLCVCallback);
        vout.attachViews();

        if (isSurfaceRecreated) {
            isSurfaceRecreated = false;
            startPaused = true;
            mPlayer.play();
        }
    }

    public void restartOutputIfNecessary() {
        if (mDisplayView == null || mDisplayView.getSurfaceTexture() == null) {
            isSurfaceRecreated = true;
            recreateOutput();
        }
    }

    protected void recreateOutput() {
        if (mPlayer != null) {
            isWaitinfForDisplay = true;
            mDisplayView = createTextureView();

            if (mDisplayView != null && mDisplayView.getSurfaceTexture() != null) {
                setVideoOutput();
            }
        }
    }

    /**
     * Releases player, VLCLib, stops and clears all the time threads
     */
    public void releaseAll() {
        if (mVLC == null || mPlayer == null) {
            return;
        }

        mPlayer.setEventListener(null);
        final IVLCVout vout = mPlayer.getVLCVout();
        vout.removeCallback(mVLCVCallback);
        vout.detachViews();

        if (mPlayer.isPlaying()) {
            mPlayer.stop();
        }
        if (!mPlayer.isReleased()) {
            mPlayer.release();
        }
        mPlayer = null;

        mVLC.release();
        mVLC = null;

        stopCounters(true);
//        mTimeUpdaters.clear();
        // Force hide loader
        if (mLoader != null && mLoader.getVisibility() == View.VISIBLE) {
            mHanler.post(new Runnable() {
                @Override
                public void run() {
                    mLoader.setVisibility(View.GONE);
                }
            });
        }

        startUp = true;

        Utils.release();
    }

    public void restart() {
        if (mPlayer == null || mPlayer.getMedia() == null || mCurrentMedia == null) {
            return;
        }
        showLoader(true);

        mDisplayView.setSurfaceTextureListener(mTextureListener);

        mStartTime = mPlayer.getTime();
        startPaused = true;
        createPlayer();
    }

    private void startCounters() {
        for (TimeUpdater updater : mTimeUpdaters.values()) {
            TIME_THREAD.removeCallbacks(updater);
            TIME_THREAD.post(updater);
        }

    }

    private void stopCounters(boolean useForce) {
        for (TimeUpdater updater : mTimeUpdaters.values()) {
            if (useForce || !updater.isLocked()) {
                TIME_THREAD.removeCallbacks(updater);
            }
        }
    }

    private void showLoader(boolean show) {
        if (mLoader != null && forceUseLoader) {
            if (show) {
                if (mLoader.getVisibility() != View.VISIBLE) {
                    mHanler.post(new Runnable() {
                        @Override
                        public void run() {
//                            Utils.animateFadeOut(mLoader);
                            mLoader.setVisibility(View.VISIBLE);
                        }
                    });
                }

            } else {
                if (mLoader.getVisibility() == View.VISIBLE) {
                    mHanler.post(new Runnable() {
                        @Override
                        public void run() {
//                            Utils.animateFadeIn(mLoader);
                            mLoader.setVisibility(View.GONE);
                        }
                    });
                }
            }

        } else {
            mRequestLoader = show;
        }
    }

    public View getDisplayView() {
        return this.mDisplayView;
    }

    @Override
    public long getCurrentPosition() {
        return (mPlayer != null) ? mPlayer.getTime() : 0l;
    }

    /**
     * Beginning the playback of the file
     *
     * @param pathToVideo path to videofile
     */
    public void startPlayback(@NonNull String pathToVideo) {
        startPlayback(pathToVideo, false);
    }

    private volatile boolean launchLock = false;

    public void startPlayback(@NonNull final String pathToVideo, final boolean startPaused) {
        if (pathToVideo.isEmpty()) {
            return;
        }

        // Should not allow fast switches because of VLC-related problems and freezes might occur
        if (launchLock) {
            Log.d(TAG, "VLC is locked for launching. Waiting...");
            mHanler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    startPlayback(pathToVideo, startPaused);
                }
            }, 100);
            return;
        } else {
            launchLock = true;
            mHanler.postDelayed(new Runnable() {
                @Override
                public void run() {
                    launchLock = false;
                }
            }, 1000);
        }

        showLoader(true);

        mCurrentMedia = Uri.parse(pathToVideo);
        this.startPaused = startPaused;
        createPlayer();
    }

    public void play() {
        if (mPlayer != null && !mPlayer.isPlaying()) {
            mPlayer.play();

        } else {
            startPaused = false;
        }
    }

    public void pause() {
        if (mPlayer != null && mPlayer.isPlaying()) {
            mPlayer.pause();

        } else {
            startPaused = true;
        }
    }

    public void stop() {
        if (mPlayer != null) {
            mPlayer.stop();
        }
    }

    public boolean isPlaying() {
        return mPlayer != null && mPlayer.isPlaying() && isPlayingStarted;
    }

    public boolean isWorking() {
        return mPlayer != null
                && mPlayer.getPlayerState() != PlayerState.IDLE
                && mPlayer.getPlayerState() != PlayerState.ERROR
                && mPlayer.getPlayerState() != PlayerState.ENDED;
    }

    public void setSpeed(float percentage) {
        if (mPlayer != null && percentage > 0.1 && percentage < 2.1 && speedPercentage != percentage) {
            speedPercentage = percentage;
            mPlayer.setRate(percentage);
        }
    }

    public long getDuration() {
        return (mPlayer != null) ? mPlayer.getLength() : 0l;
    }

    public void setStartTime(long start) {
        this.mStartTime = start;
    }

    public void seekTo(long i) {
        if (mPlayer != null && mPlayer.isSeekable() && i != mPlayer.getTime()) {
            i = (i < 0) ? 0 : (i > mPlayer.getLength()) ? mPlayer.getLength() : i;

            if (mControls != null) {
                mControls.updateCurrentTime(i);
                mControls.updateJogger((i - vidBounds[0]) / (float) (vidBounds[1] - vidBounds[0]));
            }

            if (mPlaybackListener != null) {
                mPlaybackListener.onSeekStart(mPlayer.getTime());
                mPlaybackListener.onSeekEnd(i);
            }

            rewind();
            mPlayer.setTime(i);
        }
    }

    private void rewind() {
        if (mDisplayView != null) {
            mDisplayView.setSurfaceTextureListener(mTextureListener);
        }
        mIsSeeking = true;
//        showLoader(true);
    }

    public void hide() {
        if (getVisibility() == View.VISIBLE)
            Utils.animateFadeOut(this);
    }

    public void show() {
        if (getVisibility() != View.VISIBLE)
            Utils.animateFadeIn(this);
    }

    public void hideControls() {
        if (mControls != null && !mControls.isHidden() && !lockControls) {
            forceHideControls = true;
            mControls.show(false);
        }
    }

    public void showControls() {
        if (mControls != null && mControls.isHidden() && !lockControls) {
            forceHideControls = false;
            mControls.show(true);
        }
    }

    /**
     * Setup existing video controls that were provided in XML or default ones
     *
     * @param builder
     */
    public void setUpControls(@NonNull ControlsBuilder builder) {
        if (mControls != null) {
            mControls.initControls(builder);
        }
    }

    /**
     * Setup brand new controls (that will replace any current) that are outside of player. <p><b>NOTE:</b> In such case
     * controls WILL NOT be hidden! Also, flag {@code allowControls} will be ignored</p>
     *
     * @param controlsView View that contains controls elements from the builder
     * @param builder      The builder with controls elements for the provided view
     */
    public void setUpControls(View controlsView, @NonNull ControlsBuilder builder) {
        if (mControls != null) {
            removeView(mControls.getTarget());
        }

        mControls = new VideoControls(controlsView);
        mControls.initControls(builder);
        mControls.setControlsListener(mControlsListener);
        lockControls = true;
    }

    public int getVolume() {
        if (mPlayer != null) {
            return mPlayer.getVolume();
        }

        return VOLUME_MAX;
    }

    public void setVolume(int volume) {
        volume = Math.max(Math.min(volume, VOLUME_MAX), VOLUME_MUTE);
        if (mPlayer != null) {
            mPlayer.setVolume(volume);
            mIsMuted = volume == VOLUME_MUTE;
        }
    }

    public void mute() {
        if (mPlayer != null) {
            mPlayer.setVolume(VOLUME_MUTE);
            mIsMuted = true;
        }
    }

    public void unMute() {
        if (mPlayer != null) {
            mPlayer.setVolume(VOLUME_MAX);
            mIsMuted = false;
        }
    }

    public void setPlaybackBounds(long from, long to) {
        if (from > 0 && to > from) {
            vidBounds[0] = from;
            vidBounds[1] = to;

            if (mPlayer != null) {
                if (mPlayer.getPosition() < from) {
                    seekTo(from);
                } else if (mPlayer.getPosition() > to) {
                    seekTo(to);
                }
            }

            if (mControls != null && mPlayer != null) {
                mControls.setOverallTime(vidBounds[1]);
                mControls.setMinimumTime(vidBounds[0]);
            }

        }
    }

    protected Bitmap getFrameBitmap() {
        if (frame == null || frame.isRecycled()) {
            if (isFrameSizeLocked) {
                frame = Bitmap.createBitmap(fSize[0], fSize[1], Bitmap.Config.RGB_565);

            } else {
                frame = Bitmap.createBitmap(mDisplayView.getWidth(), mDisplayView.getHeight(), Bitmap.Config.ARGB_8888);
            }
        }

        return frame;
    }

    public Bitmap getCurrentFrame() {
        if (mDisplayView != null) {
            mDisplayView.getBitmap(getFrameBitmap());
        }

        return getFrameBitmap();
    }

    /**
     * Returns the X and Y position of the display
     *
     * @return An array with X and Y positions of the display view
     */
    public float[] getDisplayOrigins() {
        return mDisplayOrigins;
    }

    public void setShowLoader(boolean show) {
        this.forceUseLoader = show;
    }

    public void setPlaybackListener(PlaybackListener l) {
        this.mPlaybackListener = l;
    }

    @Override
    public void addOnTimeListener(OnTimeUpdatedListener l, long intervalMillis, String tag) {
        addOnTimeListener(l, intervalMillis, tag, false);
    }

    public void addOnTimeListener(OnTimeUpdatedListener l, long intervalMillis, String tag, boolean isLocked) {
        mTimeUpdaters.put(tag, new TimeUpdater(l, intervalMillis, isLocked));
    }

    public void setOnErrorListener(OnErrorListener l) {
        this.mErrorListener = l;
    }

    public void setOnOpenningListener(OnOpeningListener l) {
        this.mOpeningListener = l;
    }

    /**
     * Calling this method will provoke clearing of player state. Use it with caution! TODO: Add this call were we check
     * and fail for NULL instead of dead-end
     */
    public void error() {
        mPEventListener.onEvent(new PlayerEvent(PlayerEvent.EncounteredError));
    }

    public float getCurrentSpeed() {
        return speedPercentage;
    }

    public Uri getCurrentMedia() {
        return mCurrentMedia;
    }

    public void setUpVtt(String str) {
        mControls.setVtt(str);
    }

    public interface OnErrorListener {
        void onError();
    }

    public interface OnOpeningListener {
        void onOpeningStarted();
    }

    public interface OnTimeUpdatedListener {
        void onTimeUpdated(long newTime);
    }

    protected class TimeUpdater implements Runnable {

        private OnTimeUpdatedListener listener;
        private long delay;
        private boolean isLocked;

        TimeUpdater(OnTimeUpdatedListener l, long delay, boolean locked) {
            this.listener = l;
            this.delay = delay;
            this.isLocked = locked;
        }

        @Override
        public void run() {
            if (mPlayer != null) {
                long elapsed = mPlayer.getTime();
                if (listener != null) {
                    listener.onTimeUpdated(elapsed);
                }

                TIME_THREAD.postDelayed(this, delay);
            }
        }

        public boolean isLocked() {
            return isLocked;
        }
    }
}