package com.playsight.videoplayer;

import android.support.annotation.IdRes;

/**
 * Created by Pavel Barmin on 14.12.15
 */
public class RewinderControl {
    protected long staticAmmount;
    protected boolean isForward;
    protected int id;

    public RewinderControl(@IdRes int id, long ammount) {
        this(id, ammount, ammount > 0);
    }

    public RewinderControl(@IdRes int id, long ammount, boolean forward) {
        this.staticAmmount = ammount;
        this.isForward = forward;
        this.id = id;

        if ((forward && ammount > 0) || (!forward && ammount < 0)) {
            this.staticAmmount = ammount;

        } else if (forward || ammount > 0) {
            this.staticAmmount = -ammount;

        } else {
            this.staticAmmount = ammount;
        }
    }
}
