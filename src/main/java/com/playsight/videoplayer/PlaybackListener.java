package com.playsight.videoplayer;

/**
 * Created by Pavel Barmin on 14.12.15
 */
public abstract class PlaybackListener {
    protected boolean bufferingStarted = false;
    protected boolean seekStarted = false;

    public void onStart(long time) {
    }

    public void onPause(long time) {
    }

    public void onStop(long time) {
    }

    public void onSeekStart(long time) {
        seekStarted = true;
    }

    public void onSeekEnd(long time) {
        if (seekStarted){
            seekStarted = false;
        }
    }

    public void onBufferingStart(long time) {
        bufferingStarted = true;
    }

    public void onBufferingEnd(long time) {
        if (bufferingStarted) {
            bufferingStarted = false;
        }
    }

}
