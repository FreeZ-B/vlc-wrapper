package com.playsight.videoplayer;

import android.animation.Animator;
import android.animation.ObjectAnimator;
import android.content.Context;
import android.util.DisplayMetrics;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewParent;

import java.util.WeakHashMap;

/**
 * Created by Pavel Barmin on 09.12.15
 */
class Utils {

    private final static long DEFAULT_ANIMATION_DURATION = 300l;
    private static WeakHashMap<View, ObjectAnimator> animators = new WeakHashMap<>();

    public static float dpToPx(float dp, Context context) {
        final DisplayMetrics metrics = context.getResources().getDisplayMetrics();
        return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, metrics);
    }

    public static View getTopLayout(View child) {
        ViewParent parent = child.getParent();
        if (parent != null && parent instanceof View) {
            return getTopLayout((View) parent);

        } else {
            return child;
        }
    }

    /**
     * Animating ALPHA 1->0
     *
     * @param target view to animate
     */
    public static void animateFadeIn(final View target) {
        animateFadeIn(target, DEFAULT_ANIMATION_DURATION);
    }

    /**
     * Animating ALPHA 1->0
     *
     * @param target   view to animate
     * @param duration animation duration
     */
    public static void animateFadeIn(final View target, long duration) {
        animateFadeIn(target, duration, null);
    }

    /**
     * Animating ALPHA 1->0
     *
     * @param target   view to animate
     * @param duration animation duration
     * @param l        Callback for animation cancellation/completion
     */
    public static void animateFadeIn(final View target, long duration, final OnAnimationCompletedListener l) {
        ObjectAnimator animator = animators.get(target);

        if (animator != null) {
            if (animator.isStarted() || animator.isRunning()) {
                animator.cancel();
            }
        }

        animator = ObjectAnimator.ofFloat(target, View.ALPHA, 1, 0);
        animator.setDuration(duration);
        animator.removeAllListeners();
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                if (target.getVisibility() != View.VISIBLE) {
                    target.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (target.getVisibility() != View.GONE) {
                    target.setVisibility(View.GONE);
                }

                if (l != null) {
                    l.onAnimationCompleted();
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                if (target.getVisibility() != View.GONE) {
                    target.setVisibility(View.GONE);
                }

                target.setAlpha(0);

                if (l != null) {
                    l.onAnimationCompleted();
                }
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });

        animators.put(target, animator);
        animator.start();
    }

    /**
     * Animating ALPHA 0->1
     *
     * @param target view to animate
     */
    public static void animateFadeOut(final View target) {
        animateFadeOut(target, DEFAULT_ANIMATION_DURATION);
    }

    /**
     * Animating ALPHA 1->0
     *
     * @param target   view to animate
     * @param duration animation duration
     */
    public static void animateFadeOut(final View target, long duration) {
        animateFadeOut(target, duration, null);
    }

    /**
     * Animating ALPHA 1->0
     *
     * @param target   view to animate
     * @param duration animation duration
     * @param l        Callback for animation cancellation/completion
     */
    public static void animateFadeOut(final View target, long duration, final OnAnimationCompletedListener l) {
        ObjectAnimator animator = animators.get(target);

        if (animator != null) {
            if (animator.isStarted() || animator.isRunning()) {
                animator.cancel();
            }
        }

        animator = ObjectAnimator.ofFloat(target, View.ALPHA, 0, 1);
        animator.setDuration(duration);
        animator.removeAllListeners();
        animator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationStart(Animator animator) {
                if (target.getVisibility() != View.VISIBLE) {
                    target.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void onAnimationEnd(Animator animator) {
                if (l != null) {
                    l.onAnimationCompleted();
                }
            }

            @Override
            public void onAnimationCancel(Animator animator) {
                target.setAlpha(1);

                if (l != null) {
                    l.onAnimationCompleted();
                }
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });

        animators.put(target, animator);
        animator.start();
    }

    public static void release() {
        animators.clear();
    }

    public interface OnAnimationCompletedListener {
        void onAnimationCompleted();
    }
}
