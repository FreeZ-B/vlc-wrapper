package com.playsight.videoplayer;

import android.content.Context;
import android.graphics.Matrix;
import android.graphics.RectF;
import android.os.Handler;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.load.resource.drawable.GlideDrawable;
import com.bumptech.glide.request.animation.GlideAnimation;
import com.bumptech.glide.request.target.SimpleTarget;
import com.squareup.picasso.Picasso;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by Pavel Barmin on 09.12.15
 */
public class VideoControls {

    private static final String TAG = "VideoControls";

    private static final int DEFAULT_CONTROLS_LAYOUT = R.layout.video_controls;
    private static final float THUMB_SCALE_EQUIVALENT = (float) 1.4;

    private View vTarget;

    private TextView tvCurrentTime;
    private TextView tvOverallTime;
    private SeekBar sbJogger;
    private TextView tvTimeStamp;
    private ImageView ivTimeThumbs;
    private RelativeLayout rlParentTimeThumbs;

    private long mCurrentTime;
    private long mOverallTime;
    private float mCurrentPosition; //0f..1f

    private boolean isLengthInHours = false;

    private boolean mControlsSet = false;

    private ControlsListener mListener;
    private long timeBeforeSeek;
    private boolean isShowing = false;
    private long mMinimumTime;
    private boolean touchByUser = false;

    private WebVttParser webVttParser;
    private android.view.ViewGroup.LayoutParams lpThumbs;
    private String vttUrl;

    private int newSizeWidth;
    private int newSizeHeight;
    private int originalSizeWidth;
    private int originalSizeHeight;
    private boolean isNewSizeSet = false;
    private boolean isOriginalSizeSet = false;
    private float resizeWidthCoefficient = 1;
    private float resizeHeightCoefficient = 1;

    protected VideoControls(Context context) {
        this(context, DEFAULT_CONTROLS_LAYOUT);
    }

    protected VideoControls(Context context, @LayoutRes int layoutId) {
        if (layoutId == -1) {
            layoutId = DEFAULT_CONTROLS_LAYOUT;
        }

        vTarget = View.inflate(context, layoutId, null);
        RelativeLayout.LayoutParams controlsParams = new RelativeLayout.LayoutParams(RelativeLayout.LayoutParams.MATCH_PARENT,
                ViewGroup.LayoutParams.WRAP_CONTENT);
        controlsParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM);
        vTarget.setLayoutParams(controlsParams);
        vTarget.setVisibility(View.GONE);

        if (layoutId == DEFAULT_CONTROLS_LAYOUT) {
            initControls(new ControlsBuilder()
                    .setCurrentTimeTextViewId(R.id.time_current)
                    .setOverallTimeTextViewId(R.id.time_overall)
                    .setSeekbarJoggerId(R.id.jogger));
        }
    }

    protected VideoControls(View nonBoundView) {
        this.vTarget = nonBoundView;
    }

    protected void initControls(@NonNull ControlsBuilder builder) {
        final View vCTime = vTarget.findViewById(builder.mCurrentTimeTextViewId);
        final View vOTime = vTarget.findViewById(builder.mOverallTimeTextViewId);
        final View vJogger = vTarget.findViewById(builder.mSeekbarJoggerId);
        final View vTimeStamp = vTarget.getRootView().findViewById(builder.mTimeStampTextViewId);
        final View vTimeThumbs = vTarget.getRootView().findViewById(builder.mTimeStampThumbsViewId);

        if (!addRewinders(builder.mRewinders) && vCTime == null && vOTime == null && vJogger == null) {
            Log.e(TAG, "Couldn't find controls with specified ID's!");
            vTarget.setVisibility(View.GONE);
            return;
        }

        this.mControlsSet = true;

        if (vCTime != null) {
            tvCurrentTime = (TextView) vCTime;
        }

        if (vOTime != null) {
            tvOverallTime = (TextView) vOTime;
        }

        if (vTimeStamp != null) {
            tvTimeStamp = (TextView) vTimeStamp;
        }

        if (vTimeThumbs != null) {
            ivTimeThumbs = (ImageView) vTimeThumbs;
            rlParentTimeThumbs = (RelativeLayout) ivTimeThumbs.getParent();
            lpThumbs = ivTimeThumbs.getLayoutParams();
        }

        if (vJogger != null) {
            sbJogger = (SeekBar) vJogger;
            sbJogger.setProgress(0);
            sbJogger.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                @Override
                public void onProgressChanged(SeekBar seekBar, int i, boolean byUser) {
                    touchByUser = byUser;
                    if (byUser) {
                        final float percentage = i / (float) seekBar.getMax();
                        long timeInMillis = (long) ((mOverallTime - mMinimumTime) * percentage + mMinimumTime);
                        updateTimeStampTime(timeInMillis);
                        int seekBarCenterX = getSeekBarThumbCenter(seekBar);
                        if (tvTimeStamp != null) {
                            tvTimeStamp.setX(seekBarCenterX - (tvTimeStamp.getWidth() / 2));
                        }
                        if (ivTimeThumbs != null) {

                            if (webVttParser != null && ivTimeThumbs.getDrawable() != null) {

                                String subtitle = webVttParser.getSubtitle(timeInMillis);
                                if (subtitle != null && !subtitle.isEmpty()) {
                                    List<Integer> coord = parseCoordinate(subtitle);

                                    lpThumbs.width = (int) Math.round(coord.get(2) * THUMB_SCALE_EQUIVALENT * resizeWidthCoefficient);
                                    lpThumbs.height = (int) Math.round(coord.get(3) * THUMB_SCALE_EQUIVALENT * resizeHeightCoefficient);
                                    ivTimeThumbs.setLayoutParams(lpThumbs);

                                    Matrix imageMatrix = ivTimeThumbs.getImageMatrix();
                                    Matrix matrix = getMatrixOfCoordinate(imageMatrix, coord);
                                    ivTimeThumbs.setImageMatrix(matrix);
                                    rlParentTimeThumbs.setX(seekBarCenterX - (rlParentTimeThumbs.getWidth() / 2));
                                    ivTimeThumbs.invalidate();
                                    rlParentTimeThumbs.invalidate();
                                }
                            }
                        }

                    }
                }

                @Override
                public void onStartTrackingTouch(SeekBar seekBar) {
                    timeBeforeSeek = mCurrentTime;
                    if (tvTimeStamp != null) {
                        tvTimeStamp.setVisibility(View.VISIBLE);
                    }
                    if (rlParentTimeThumbs != null && ivTimeThumbs.getDrawable() != null) {
                        rlParentTimeThumbs.setVisibility(View.VISIBLE);
                    }
                }

                @Override
                public void onStopTrackingTouch(SeekBar seekBar) {
                    touchByUser = false;
                    if (tvTimeStamp != null) {
                        tvTimeStamp.setVisibility(View.GONE);
                    }
                    if (rlParentTimeThumbs != null && ivTimeThumbs.getDrawable() != null) {
                        rlParentTimeThumbs.setVisibility(View.GONE);
                    }
                    if (mListener != null) {
                        float percentage = seekBar.getProgress() / (float) seekBar.getMax();
                        if (!mListener.onJoggerChanged(percentage)) {
                            // If we cant seek - roll back
                            updateCurrentTime(timeBeforeSeek);
                            percentage = timeBeforeSeek / mOverallTime;
                            seekBar.setProgress((int) (seekBar.getMax() * percentage));
                        }
                    }
                }
            });
        }
    }

    private Matrix getMatrixOfCoordinate(Matrix matrix, List<Integer> coord) {
        RectF drawableRect = new RectF(coord.get(0), coord.get(1), coord.get(0) + coord.get(2), coord.get(1) + coord.get(3));
        RectF viewRect = new RectF(0, 0, coord.get(2), coord.get(3));
        matrix.setRectToRect(drawableRect, viewRect, Matrix.ScaleToFit.CENTER);

        matrix.postScale(THUMB_SCALE_EQUIVALENT * resizeWidthCoefficient, THUMB_SCALE_EQUIVALENT * resizeWidthCoefficient);
        return matrix;
    }

    private List<Integer> parseCoordinate(String subtitle) {
        int startCoordinateIndex = subtitle.indexOf("#xywh=");
        String noneParseCoord = subtitle.substring(startCoordinateIndex);

        Pattern p = Pattern.compile("-?\\d+");
        Matcher m = p.matcher(noneParseCoord);
        List<Integer> coord = new ArrayList<Integer>();
        while (m.find()) {
            coord.add(Integer.valueOf(m.group()));
        }
        for (int i = coord.size(); i < 4; i++) {
            coord.add(0);
        }

        for (int i = 0; i < coord.size(); i++) {
            if (coord.get(i) != 0 && (i == 0 || i == 2)) {
                coord.set(i, (int) (coord.get(i) / resizeWidthCoefficient));
            }
            if (coord.get(i) != 0 && (i == 1 || i == 3)) {
                coord.set(i, (int) (coord.get(i) / resizeHeightCoefficient));
            }
        }
        return coord;
    }

    private int getSeekBarThumbCenter(SeekBar seekBar) {
        int[] l = new int[2];
        seekBar.getLocationOnScreen(l);

        int width = seekBar.getWidth() - seekBar.getPaddingLeft() - seekBar.getPaddingRight();
        int thumbPos = seekBar.getPaddingLeft() + width * seekBar.getProgress() / seekBar.getMax();
        return l[0] + thumbPos;
    }


    protected void updateJogger(float percentage) {
        if (percentage != this.mCurrentPosition && !touchByUser) {
            mCurrentPosition = percentage;
            if (sbJogger != null && !Double.isNaN(percentage) && !Double.isInfinite(percentage)) {
                sbJogger.setProgress(new BigDecimal(percentage).setScale(3, BigDecimal.ROUND_FLOOR)
                        .multiply(new BigDecimal(sbJogger.getMax())).intValue());
            }
        }
    }

    private String getTime(long milis) {
        long time = milis / 1000;

        long m = time / 60;
        long h = m / 60;
        m = m % 60;
        long s = time % 60;

        if (h > 0 || isLengthInHours) {
            if (!isLengthInHours) {
                isLengthInHours = true;
            }

            return String.format("%02d:%02d:%02d", h, m, s);

        } else {
            return String.format("%02d:%02d", m, s);
        }
    }

    protected void show(boolean show) {
        if (mControlsSet) {
            if (show) {
                isShowing = true;
                Utils.animateFadeOut(vTarget);

                if (vTarget.findViewById(ivTimeThumbs.getId()) == null) {
                    Utils.animateFadeOut(ivTimeThumbs);
                }

            } else {
                isShowing = false;
                Utils.animateFadeIn(vTarget);

                if (vTarget.findViewById(ivTimeThumbs.getId()) == null) {
                    Utils.animateFadeIn(ivTimeThumbs);
                }
            }
        }
    }

    protected View getTarget() {
        return vTarget;
    }

    protected void setControlsListener(ControlsListener l) {
        this.mListener = l;
    }

    protected void updateCurrentTime(long milis) {
        if (milis != this.mCurrentTime) {
            mCurrentTime = milis;
            if (tvCurrentTime != null) {
                tvCurrentTime.setText(getTime(mCurrentTime));
            }

        }
    }

    private void updateTimeStampTime(long milis) {
        if (tvTimeStamp != null) {
            tvTimeStamp.setText(getTime(milis));
        }
    }

    protected void setOverallTime(long milis) {
        if (milis != this.mOverallTime) {
            mOverallTime = milis;
            if (sbJogger != null) {
                sbJogger.setMax((int) (milis / 1000));
            }
            if (tvOverallTime != null) {
                tvOverallTime.setText(getTime(mOverallTime));
            }
        }
    }

    protected void setMinimumTime(long milis) {
        if (milis > 0 && milis < this.mOverallTime) {
            mMinimumTime = milis;
            if (sbJogger != null) {
                sbJogger.setMax((int) ((mOverallTime - mMinimumTime) / (float) 1000));
            }
        }
    }

    protected void setVtt(String uri) {
        vttUrl = uri;
        prepareThumbsView(vttUrl);
    }

    private void prepareThumbsView(final String uriStr) {
        webVttParser = new WebVttParser(uriStr, new WebVttParser.SubtitleListener() {
            @Override
            public void VttFilePrepared() {


                String param = webVttParser.getSubtitle(0);
                if (param != null && !param.isEmpty()) {
                    String thumbFilePath = uriStr.substring(0, uriStr.lastIndexOf('/') + 1);
                    String fileName = param.substring(0, param.indexOf("#xywh"));
                    thumbFilePath = thumbFilePath + fileName;

                    Handler mainHandler = new Handler(ivTimeThumbs.getContext().getMainLooper());

                    final String finalThumbFilePath = thumbFilePath;

                    Runnable myRunnable = new Runnable() {
                        @Override
                        public void run() {

                            Glide.with(ivTimeThumbs.getContext())
                                    .load(finalThumbFilePath)
                                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                                    .skipMemoryCache(true)
                                    .into(new SimpleTarget<GlideDrawable>() {
                                        @Override
                                        public void onResourceReady(GlideDrawable glideDrawable, GlideAnimation<? super GlideDrawable> glideAnimation) {
                                            originalSizeWidth = glideDrawable.getIntrinsicWidth();
                                            originalSizeHeight = glideDrawable.getIntrinsicHeight();
                                            isOriginalSizeSet = true;

                                            newSizeWidth = originalSizeWidth;
                                            newSizeHeight = originalSizeHeight;
                                            while (newSizeWidth >= 4096 || newSizeHeight >= 4096) {
                                                newSizeWidth = (int) (newSizeWidth * 0.9f);
                                                newSizeHeight = (int) (newSizeHeight * 0.9f);
                                            }
                                            isOriginalSizeSet = true;
                                            calculateResizeEquivalent();

                                            Picasso.with(ivTimeThumbs.getContext())
                                                    .load(finalThumbFilePath)
                                                    .resize(newSizeWidth, newSizeHeight)
                                                    .into(ivTimeThumbs);

                                        }
                                    });

                        }
                    };
                    mainHandler.post(myRunnable);

                }
            }
        });
    }

    private synchronized void calculateResizeEquivalent() {
        if (isOriginalSizeSet) {
            resizeWidthCoefficient = originalSizeWidth / (float) newSizeWidth;
            resizeHeightCoefficient = originalSizeHeight / (float) newSizeHeight;
        }
    }

    public boolean isHidden() {
        return !isShowing;
    }

    protected boolean addRewinders(List<RewinderControl> rewinders) {
        boolean isAtListOneEnabled = false;
        if (rewinders != null && !rewinders.isEmpty()) {
            for (final RewinderControl rc : rewinders) {
                View rewinder = vTarget.findViewById(rc.id);
                if (rewinder != null) {
                    if (!isAtListOneEnabled) {
                        isAtListOneEnabled = true;
                    }

                    rewinder.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            if (mListener != null) {
                                mListener.onRewind(rc.staticAmmount);
                            }
                        }
                    });
                }
            }
        }

        return isAtListOneEnabled;
    }

    public long getOverallTime() {
        return this.mOverallTime;
    }

    protected interface ControlsListener {
        boolean onJoggerChanged(float newPercentage);

        void onRewind(long ms);
    }

}
