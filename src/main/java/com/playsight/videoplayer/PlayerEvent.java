package com.playsight.videoplayer;

import org.videolan.libvlc.MediaPlayer;

/**
 * Created by Pavel Barmin on 14.12.15
 */
public class PlayerEvent extends MediaPlayer.Event {
    public PlayerEvent(int type) {
        super(type);
    }

    public PlayerEvent(int type, long arg1) {
        super(type, arg1);
    }

    public PlayerEvent(int type, float arg2) {
        super(type, arg2);
    }
}
